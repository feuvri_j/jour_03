#ifndef BC_H_
# define BC_H_

#include <stdlib.h>

void	bc_write_char(char);
void	bc_write_str(char *);
void	bc_write_nb(int);
char	*bc_get_line(int);
char	bc_get_char(void);
void	bc_init_rand(void);
int	bc_rand(int, int);

#endif /* !BC_H_ */
