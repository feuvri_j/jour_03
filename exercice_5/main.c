#include <string.h>
#include "bc.h"

char	*evil(char *);

int	main(void)
{
  char	*str;

  bc_write_str("vous brulerez dans les flammes de l'enfer ! -> ");
  str = evil("vous brulerez dans les flammes de l'enfer !");
  bc_write_str(str);
  bc_write_str(" : ");
  if (strcmp(str, "vos bruularaz kans la flamma ka l'anfarr !") == 0)
    bc_write_str("Ok !\n");
  else
    bc_write_str("Erreur !\n");
  free(str);
  bc_write_str("uresdur derkrrr -> ");
  str = evil("uresdur derkrrr");
  bc_write_str(str);
  bc_write_str(" : ");
  if (strcmp(str, "uurakuurr karkrrrr") == 0)
    bc_write_str("Ok !\n");
  else
    bc_write_str("Erreur !\n");
  free(str);
  bc_write_str("ouodrdees -> ");
  str = evil("ouodrdees");
  bc_write_str(str);
  bc_write_str(" : ");
  if (strcmp(str, "ookrkaa") == 0)
    bc_write_str("Ok !\n");
  else
    bc_write_str("Erreur !\n");
  free(str);
  return (0);
}
