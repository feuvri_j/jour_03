#include "bc.h"

int	main(void)
{
  int	a;
  int	b;

  a = 30000;
  b = 60000;
  bc_write_str("Avant l'échange :\n");
  bc_write_str("--- argent des Lannisters : ");
  bc_write_nb(a);
  bc_write_str("\n--- argent des Hautjardins : ");
  bc_write_nb(b);
  echange(&a, &b);
  bc_write_str("\nAprès :\n");
  bc_write_str("--- argent des Lannisters : ");
  bc_write_nb(a);
  bc_write_str("\n--- argent des Hautjardins : ");
  bc_write_nb(b);
  bc_write_char('\n');
  return (0);
}
