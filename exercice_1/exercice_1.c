#include "bc.h"
#include <stdlib.h>

void echange(int *h, int *m)
{
    int memoire;
    memoire = *h;
    *h = *m;
    *m = memoire;
}