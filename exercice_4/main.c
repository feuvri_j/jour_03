#include "bc.h"

int	fact(int);

int	main(void)
{
  int	nb;

  nb = 0;
  bc_write_str("Factorielle de 0 : ");
  nb = fact(0);
  bc_write_nb(nb);
  if (nb == 1)
    bc_write_str(" ----> Ok\n");
  else
    bc_write_str(" ----> Error\n");
  bc_write_str("Factorielle de 1 : ");
  nb = fact(1);
  bc_write_nb(nb);
  if (nb == 1)
    bc_write_str(" ----> Ok\n");
  else
    bc_write_str(" ----> Error\n");
  bc_write_str("Factorielle de 8 : ");
  nb = fact(8);
  bc_write_nb(nb);
  if (nb == 40320)
    bc_write_str(" ----> Ok\n");
  else
    bc_write_str(" ----> Error\n");
  return (0);
}
