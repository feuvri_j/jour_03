#include "bc.h"

void	say(char *);

int	main(void)
{
  say("You know nothing, Jon Snow.\n");
  say(NULL);
  return (0);
}
